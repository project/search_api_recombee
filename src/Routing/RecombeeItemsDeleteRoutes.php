<?php

namespace Drupal\search_api_recombee\Routing;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Provides dynamic routes for search_api_recombee.
 */
class RecombeeItemsDeleteRoutes implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new recombee remove_items route subscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  final public function __construct(PluginManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Define routes for all recombee remove_items plugins.
   */
  public function routes() {
    $search_api_servers = $this->entityTypeManager->getStorage('search_api_server')->loadMultiple();
    $routes = [];
    foreach ($search_api_servers as $search_api_server) {
      if ($search_api_server->getBackendId() === 'recombee') {
        $routes['entity.search_api_server.remove_items'] = new Route(
          "admin/config/search/search-api/server/{search_api_server}/remove-items",
          [
            '_entity_form' => 'search_api_server.remove_items',
            '_title' => 'Remove Recombee Items',
          ],
          [
            '_permission' => 'remove recombee items',
          ]
        );
      }
    }
    return $routes;
  }

}
