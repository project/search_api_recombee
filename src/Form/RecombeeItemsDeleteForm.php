<?php

namespace Drupal\search_api_recombee\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Psr\Log\LoggerInterface;
use Recombee\RecommApi\Client;
use Recombee\RecommApi\Exceptions\ApiException;
use Recombee\RecommApi\Requests\DeleteItem;
use Recombee\RecommApi\Requests\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for deleting items from Recombee.
 *
 * @internal
 */
class RecombeeItemsDeleteForm extends EntityForm {

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * A messenger instance.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  final public function __construct(LoggerInterface $logger, MessengerInterface $messenger) {
    $this->logger = $logger;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')->get('search_api_recombee'),
      $container->get('messenger')
    );
  }

  /**
   * The Recombee API client.
   *
   * @var \Recombee\RecommApi\Client
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('The Recombee UI does not currently support the ability to delete items in the database. This can cause problems when the item exists in Recombee but not in Drupal. Use this form to remove items from Recombee.'),
    ];
    $form['recombee_item_ids'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Recombee Item IDs'),
      '#description' => $this->t('Use this to specify the Recombee Item Ids in new lines.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actionsElement(array $form, FormStateInterface $form_state) {
    $form = parent::actionsElement($form, $form_state);
    $form['submit']['#value'] = $this->t('Delete');
    unset($form['delete']);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\search_api\ServerInterface $server */
    $server = $this->getEntity();
    if ($server->getBackendId() === 'recombee' && $server->hasValidBackend()) {
      $config = $server->getBackendConfig();
      $account = $config['account'];
      $token = $config['token'];
      $region = $config['region'];
      // Get the submitted recombee item ids.
      $recombee_item_ids = $form_state->getValue('recombee_item_ids');
      $item_ids = array_filter(explode(PHP_EOL, $recombee_item_ids));
      $success = [];
      $failed = [];
      $message = '';
      if (!empty($account) && !empty($token) && !empty($region) && !empty($item_ids)) {
        foreach ($item_ids as $item_id) {
          // Call API to delete the item.
          try {
            $this->send(new DeleteItem(trim($item_id)), $account, $token, $region);
            $success[] = trim($item_id);
          }
          catch (\Exception $e) {
            $failed[] = trim($item_id);
            $this->logger->error($e->getMessage());
            $failed_message = $this->t('@item_id was not deleted:', ['@item_id' => trim($item_id)]);
            $this->messenger->addError($failed_message . $e->getMessage());
          }
        }
        if (!empty($success) && count($success) > 0) {
          $message .= $this->formatPlural(count($success), 'The delete operation has been completed. @success item removed.', 'The delete operation has been completed. @success items removed.', ['@success' => count($success)]);
        }
        if (!empty($failed) && count($failed) > 0) {
          $message .= $this->formatPlural(count($failed), '@failed item failed.', '@failed items failed.', ['@failed' => count($failed)]);
        }
        if ((!empty($success) && count($success) > 0) || (!empty($failed) && count($failed) > 0)) {
          $this->messenger->addStatus($message);
        }
      }
    }
  }

  /**
   * Sends a Recombee API request.
   *
   * @param \Recombee\RecommApi\Requests\Request $request
   *   The Recombee API request to be sent.
   * @param string $account
   *   The Recombee API database ID.
   * @param string $token
   *   The Recombee API private token.
   * @param string $region
   *   The Recombee Database region.
   *
   * @return array
   *   The Recombee API call result.
   *
   * @throws \Recombee\RecommApi\Exceptions\ApiException
   *   If the Recombee API error occurs.
   */
  protected function send(Request $request, $account, $token, $region) {
    // Initialize a temporary API client if needed.
    $client = $this->getClient($account, $token, $region);

    try {
      // Send the API request.
      $request->setTimeout(10000);
      $result = $client->send($request);
    }
    catch (ApiException $e) {
      // Parse error if available.
      $error = json_decode($e->getMessage());
      if (!empty($error->message)) {

        // Compose message to be displayed.
        $error_message = Markup::create($error->message);
        $message = $this->t('Recombee API error @code: @message', [
          '@code' => !empty($error->statusCode) ? $error->statusCode : 0,
          '@message' => $error_message,
        ]);
        $this->logger->error($message);
        throw new ApiException($message);
      }
      else {
        throw new ApiException($e);
      }
    }
    return $result;
  }

  /**
   * Gets the Recombee API client.
   *
   * @param string $account
   *   The Recombee API database ID.
   * @param string $token
   *   The Recombee API private token.
   * @param string $region
   *   The Recombee Database region.
   *
   * @return \Recombee\RecommApi\Client
   *   The Recombee API client.
   */
  protected function getClient($account, $token, $region) {
    // Only if not initialized yet.
    if (empty($this->client)) {
      // Initialize API client.
      $this->client = new Client($account, $token, [
        'protocol' => 'https',
        'serviceName' => 'drupal-search-api-recombee',
        'region' => $region,
      ]);
    }
    return $this->client;
  }

}
